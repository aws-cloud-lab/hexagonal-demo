const jwt = require('jsonwebtoken');
const AWS = require('aws-sdk');

exports.handler = async (event) => {
  const { userId, email, password } = JSON.parse(event.body);
  const dynamodb = new AWS.DynamoDB.DocumentClient();

  const params = {
    TableName: 'users',
    Key: {
      userId: userId,
      email: email
    }
  };

  try {
    const result = await dynamodb.get(params).promise();
    const user = result.Item;

    if (!user) {
      return { statusCode: 404, body: 'User not found' };
    }

    if (user.password !== password) {
      return { statusCode: 401, body: 'Incorrect password' };
    }

    const token = jwt.sign({ userId: user.userId }, process.env.JWT_SECRET, {
      expiresIn: '1h'
    });

    console.log(token);

    return { statusCode: 200, body: JSON.stringify({ token: token }) };
  } catch (error) {
    return { statusCode: 500, body: JSON.stringify(error) };
  }
};
