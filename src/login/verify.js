const jwt = require('jsonwebtoken');

exports.handler = (event, context, callback) => {
  const token = event.headers.Authorization;
  console.log("--- token --- ", token);
  jwt.verify(token, process.env.JWT_SECRET, (error, decoded) => {
    if (error) {
      callback(error);
    } else {
      callback(null, { userId: decoded.userId });
    }
  });
};
