const AWS = require('aws-sdk');
const sqs = require('/opt/nodejs/util/sqs-fle');

exports.handler = async (event) => {
  const dynamodb = new AWS.DynamoDB.DocumentClient();
  const { userId, name, email, city, password } = JSON.parse(event.body);

  const params = {
    TableName: 'users',
    Item: {
      userId: userId,
      f_name: name,
      email: email,
      city: city,
      password: password
    }
  };

  try {
    // SQS
    const queue = process.env.SQS;
    const attr = {
      "WeeksOn": {
        DataType: "Number",
        StringValue: "6"
      }
    };
    const message = "Hello World!";
    const sendMessageStatus = await sqs.sendMessage(queue, attr, message);
    console.log(sendMessageStatus);

    // const sqsMessages = await sqs.getMessages(queue, 10);
    // console.log(sqsMessages);

    console.log("Saved");
    return { statusCode: 200, body: JSON.stringify(message) };
  } catch (error) {
    return { statusCode: 500, body: JSON.stringify(error) };
  }
};
