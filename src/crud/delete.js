const AWS = require('aws-sdk');

exports.handler = async (event) => {
  const dynamodb = new AWS.DynamoDB.DocumentClient();
  const { userId, email } = JSON.parse(event.body);

  const params = {
    TableName: 'users',
    Key: {
      userId: userId,
      email: email
    }
  };

  try {
    await dynamodb.delete(params).promise();
    return { statusCode: 204 };
  } catch (error) {
    return { statusCode: 500, body: JSON.stringify(error) };
  }
};
