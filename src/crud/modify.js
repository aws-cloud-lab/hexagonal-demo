const AWS = require('aws-sdk');

exports.handler = async (event) => {
  const dynamodb = new AWS.DynamoDB.DocumentClient();
  const { userId, name, email, city } = JSON.parse(event.body);

  const params = {
    TableName: 'users',
    Key: {
      userId: userId,
      email: email
    },
    UpdateExpression: 'SET f_name = :f_name, city = :city',
    ExpressionAttributeValues: {
      ':f_name': name,
      ':city': city
    },
    ReturnValues: 'ALL_NEW'
  };

  try {
    const result = await dynamodb.update(params).promise();
    return { statusCode: 200, body: JSON.stringify(result.Attributes) };
  } catch (error) {
    return { statusCode: 500, body: JSON.stringify(error) };
  }
};
