import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as apigateway from 'aws-cdk-lib/aws-apigateway';
import * as dynamodb from 'aws-cdk-lib/aws-dynamodb';
import * as iam from 'aws-cdk-lib/aws-iam';
import * as kms from 'aws-cdk-lib/aws-kms';
import * as sqs from 'aws-cdk-lib/aws-sqs';

export class UserManagementStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const userKey = new kms.Key(this, 'userTableKey', {
      description: 'KMS keys for User Management',
      alias: 'table/users',
      enableKeyRotation: true
    });

    // SQS
    const userManagementQueue = new sqs.Queue(this, 'user-queue', {
      queueName: 'user-sqs.fifo',
      fifo: true,
      contentBasedDeduplication: true,
      // deliveryDelay: cdk.Duration.seconds(10)
    })

    const userTable = new dynamodb.Table(this, 'users-table', {
      tableName: 'users',
      partitionKey: {
        name: 'userId',
        type: dynamodb.AttributeType.STRING
      },
      sortKey: {
        name: 'email',
        type: dynamodb.AttributeType.STRING
      },
      encryption: dynamodb.TableEncryption.CUSTOMER_MANAGED,
      encryptionKey: userKey
    });

    const createUser = new lambda.Function(this, 'createUser-Lambda', {
      runtime: lambda.Runtime.NODEJS_16_X,
      code: lambda.Code.fromAsset('src/crud'),
      handler: 'create.handler',
      functionName: 'create-user',
      environment: {
        'KMS': userKey.keyId,
        'SQS': userManagementQueue.queueUrl
      },
      layers: [
        lambda.LayerVersion.fromLayerVersionArn(this, 'createUser-base-layer', 'arn:aws:lambda:____:_____:layer:base-layer:12')
      ]
    });

    const deleteUser = new lambda.Function(this, 'deleteUser-Lambda', {
      runtime: lambda.Runtime.NODEJS_16_X,
      code: lambda.Code.fromAsset('src/crud'),
      handler: 'delete.handler',
      functionName: 'delete-user',
      environment: {
        'KMS': userKey.keyId,
        'SQS': userManagementQueue.queueUrl
      },
      layers: [
        lambda.LayerVersion.fromLayerVersionArn(this, 'deleteUser-base-layer', 'arn:aws:lambda:____:_____:layer:base-layer:12')
      ]
    });

    const modifyUser = new lambda.Function(this, 'modifyUser-Lambda', {
      runtime: lambda.Runtime.NODEJS_16_X,
      code: lambda.Code.fromAsset('src/crud'),
      handler: 'modify.handler',
      functionName: 'modify-user',
      environment: {
        'KMS': userKey.keyId,
        'SQS': userManagementQueue.queueUrl
      },
      layers: [
        lambda.LayerVersion.fromLayerVersionArn(this, 'modifyUser-base-layer', 'arn:aws:lambda:____:_____:layer:base-layer:12')
      ]
    });

    // subscribe users
    const subscribeUser = new lambda.Function(this, 'subscribeUser-Lambda', {
      runtime: lambda.Runtime.NODEJS_16_X,
      code: lambda.Code.fromAsset('src/crud'),
      handler: 'subscribe.handler',
      environment: {
        'KMS': userKey.keyId,
        'SQS': userManagementQueue.queueUrl
      },
      layers: [
        lambda.LayerVersion.fromLayerVersionArn(this, 'subscribeUser-base-layer', 'arn:aws:lambda:____:_____:layer:base-layer:12')
      ]
    });

    const api = new apigateway.RestApi(this, 'userManagement-apigw', {
      restApiName: 'UserManagement'
    });

    const users = api.root.addResource('users');

    users.addMethod('POST', new apigateway.LambdaIntegration(createUser));
    users.addMethod('DELETE', new apigateway.LambdaIntegration(deleteUser));
    users.addMethod('PUT', new apigateway.LambdaIntegration(modifyUser));

    createUser.addToRolePolicy(new iam.PolicyStatement({
      actions: ['dynamodb:*', "kms:*", 'sqs:*'],
      resources: [userTable.tableArn, userKey.keyArn, userManagementQueue.queueArn]
    }));

    deleteUser.addToRolePolicy(new iam.PolicyStatement({
      actions: ['dynamodb:*', "kms:*", 'sqs:*'],
      resources: [userTable.tableArn, userKey.keyArn, userManagementQueue.queueArn]
    }));

    modifyUser.addToRolePolicy(new iam.PolicyStatement({
      actions: ['dynamodb:*', "kms:*", 'sqs:*'],
      resources: [userTable.tableArn, userKey.keyArn, userManagementQueue.queueArn]
    }));

    subscribeUser.addToRolePolicy(new iam.PolicyStatement({
      actions: ['dynamodb:*', "kms:*", 'sqs:*'],
      resources: [userTable.tableArn, userKey.keyArn, userManagementQueue.queueArn]
    }));

    // login
    const loginUser = new lambda.Function(this, 'loginUser-Lambda', {
      runtime: lambda.Runtime.NODEJS_16_X,
      code: lambda.Code.fromAsset('src/login'),
      handler: 'login.handler',
      timeout: cdk.Duration.seconds(60),
      functionName: 'login-user',
      environment: {
        'JWT_SECRET': 'test'
      }
    });
    loginUser.addToRolePolicy(new iam.PolicyStatement({
      actions: ['dynamodb:*'],
      resources: [userTable.tableArn]
    }));

    const verifyUserLogin = new lambda.Function(this, 'verifyUserLogin-Lambda', {
      runtime: lambda.Runtime.NODEJS_16_X,
      code:lambda.Code.fromAsset('src/login'),
      handler: 'verify.handler',
      environment: {
        'JWT_SECRET': 'test'
      },
      functionName: 'verify-user-login'
    });
    verifyUserLogin.addToRolePolicy(new iam.PolicyStatement({
      actions: ['dynamodb:*'],
      resources: [userTable.tableArn]
    }));

    const login = api.root.addResource('login');
    login.addMethod('POST', new apigateway.LambdaIntegration(loginUser));

    const verify = api.root.addResource('verify');
    verify.addMethod('POST', new apigateway.LambdaIntegration(verifyUserLogin));
  }
}
